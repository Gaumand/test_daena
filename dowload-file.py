import urllib.request

print('Beginning file download ...')

url = 'https://ted.europa.eu/xml-packages/daily-packages/2019/07/20190725_2019142.tar.gz'
urllib.request.urlretrieve(url, '/tmp/test.zip')

print('Finished file download.')